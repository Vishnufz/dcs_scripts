def calculate_solar_panel_specifications(panel_type, energy_consumption=500, roof_space=50, solar_irradiance=1100):
    # Define panel specifications including price per watt
    #Average hours of sunlight per day in Austria/Germany/US: 5 Hrs
    #Average energy consumption in Austria is about 4500 KWH per year or around 375 KWH per month
    #Average panel solar panel efficiency is 15 -20 %
    #
    panel_specs = {
        "Type1:Low efficiency low cost": {"area": 1.5, "wattage": 300, "efficiency": 0.2, "price_per_watt": 4},
        "Type2:Medium efficiency and medium cost": {"area": 1.7, "wattage": 400, "efficiency": 0.2, "price_per_watt": 4.5},
        "Type3:High efficiency and high cost": {"area": 2, "wattage": 500, "efficiency": 0.2, "price_per_watt": 5}
    }

    # Get panel specifications based on the selected type
    selected_panel = panel_specs.get(panel_type)
    if not selected_panel:
        print("Invalid panel type selected.")
        return None

    panel_area = selected_panel["area"]
    panel_wattage = selected_panel["wattage"]
    panel_efficiency = selected_panel["efficiency"]
    price_per_watt = selected_panel["price_per_watt"]

    # Calculate system size
    sunlight_per_day = 5
    system_size_kw = energy_consumption / (30 * sunlight_per_day)  # Assuming 5 hours of sunlight per day on average

    # Calculate panel area required
    panel_area_sqm = system_size_kw / (panel_efficiency * panel_wattage / 1000)
    if roof_space <= panel_area_sqm :
        print("Roof area is less than that is required for your panel installation!")
        print("Select a different panel type and check again!")
        exit()

    # Calculate total cost
    total_cost_low = system_size_kw * panel_wattage * price_per_watt
    total_cost_high = total_cost_low  # For simplicity, let's assume the high estimate is the same as the low estimate

    # Calculate number of panels required based on available roof space
    num_panels_required = panel_area_sqm / panel_area

    return system_size_kw, panel_area_sqm, total_cost_low, total_cost_high, num_panels_required


def main():
    print("Welcome to the Solar Panel Calculator!")

    # Panel types
    panel_types = ["Type1:Low efficiency low cost", "Type2:Medium efficiency and medium cost", "Type3:High efficiency and high cost"]
    print("Available panel types:")
    for i, panel_type in enumerate(panel_types, start=1):
        print(f"{i}. {panel_type}")

    # Get user choice for panel type
    choice = input("Choose a panel type (1/2/3): ").strip()
    if choice not in ["1", "2", "3"]:
        print("Invalid choice.")
        return

    panel_type = panel_types[int(choice) - 1]

    # Get user input for other parameters
    energy_consumption = float(input("Enter your average monthly energy consumption (kWh) [default: 500]: ") or 500)
    roof_space = float(input("Enter the available roof space (square meters) [default: 50]: ") or 50)
    solar_irradiance = float(
        input("Enter the average solar irradiance in your location (kWh/m²/year) [default: 1100]: ") or 1100)

    # Calculate specifications
    specs = calculate_solar_panel_specifications(panel_type, energy_consumption, roof_space, solar_irradiance)
    if specs:
        # Print the results
        system_size_kw, panel_area_sqm, total_cost_low, total_cost_high, num_panels_required = specs
        print("\nSolar Panel System Specifications:")
        print(f"System Size: {system_size_kw:.2f} kW")
        print(f"Panel Area: {panel_area_sqm:.2f} square meters")
        print("\nApproximate Cost:")
        print(f"Low Estimate: €{total_cost_low:.2f}")
        print(f"High Estimate: €{total_cost_high:.2f}")
        print(f"\nNumber of Panels Required: {num_panels_required:.0f}")


if __name__ == "__main__":
    main()