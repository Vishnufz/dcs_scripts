This document provides a detailed explanation of the Python script for calculating solar panel specifications based on user input.
The below assumptions are used in the code: 
1. Average hours of sunlight per day in Austria/Germany/US: 5 Hrs 
2. Average energy consumption in Austria is about 4500 KWH per year or around 375 KWH per month 
3. Average panel solar panel efficiency is 15 -20 %

## 1. Function Definitions

### `calculate_solar_panel_specifications(panel_type, energy_consumption=500, roof_space=50, solar_irradiance=1100)`

- This function calculates the specifications of solar panels based on the input parameters.
- It defines a dictionary `panel_specs` containing specifications for different panel types.
- ```python
      panel_specs = {
        "Type1:Low efficiency low cost": {"area": 1.5, "wattage": 300, "efficiency": 0.2, "price_per_watt": 4},
        "Type2:Medium efficiency and medium cost": {"area": 1.7, "wattage": 400, "efficiency": 0.2, "price_per_watt": 4.5},
        "Type3:High efficiency and high cost": {"area": 2, "wattage": 500, "efficiency": 0.2, "price_per_watt": 5}
    }

- Retrieves panel specifications based on the selected panel type.
- Calculates the panel area required based on system size and panel efficiency.
- Checks if the available roof space is sufficient for panel installation.
- Calculates the total cost based on system size, panel wattage, and price per watt.
- Calculates the number of panels required based on available roof space.
- Returns the calculated specifications.

## 2. Main Function

### `main()`

- Entry point of the script.
- Displays available panel types and prompts the user to choose one.
- Prompts the user for energy consumption, available roof space, and solar irradiance.
- Calls `calculate_solar_panel_specifications()` to compute solar panel specifications.
- Prints the results including system size, panel area, total cost estimates, and number of panels required.

## 3. Execution

- The script executes `main()` when run directly.
