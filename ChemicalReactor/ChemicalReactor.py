import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#Simulation parameters
t_start = 0            # Start time
t_end = 100            # End time
initial_concentration_A = 1.0  # Initial concentration of A (in moles/volume)
initial_concentration_B = 0.5  # Initial concentration of B (in moles/volume)
initial_concentration_C = 0    # Initial concentration of C (in moles/volume)
rate_constant = 0.05  # Rate constant (in volume/(moles.time))

#Define the system of differential equations
def reactionODEs(y, t):
    A, B, C = y
    dA_dt = -rate_constant * A * B
    dB_dt = -rate_constant * A * B
    dC_dt = rate_constant * A * B
    return [dA_dt, dB_dt, dC_dt]

#Set up the initial conditions
initial_conditions = [initial_concentration_A, initial_concentration_B, initial_concentration_C]

#Time points for simulation
t = np.linspace(t_start, t_end, 500)

#Solve the ODEs numerically
concentrations = odeint(reactionODEs, initial_conditions, t)

#Extract concentrations of A, B, and C from concentrations array
concentration_A = concentrations[:, 0]
concentration_B = concentrations[:, 1]
concentration_C = concentrations[:, 2]

#Plot the concentrations vs. time
plt.plot(t, concentration_A, 'r', label='[A]')
plt.plot(t, concentration_B, 'b', label='[B]')
plt.plot(t, concentration_C, 'g', label='[C]')
plt.xlabel('Time (s)')
plt.ylabel('Concentration')
plt.legend()
plt.title('Batch Reactor Simulation with Second-Order Reaction')
plt.show()
# Save the plot as PNG file
plt.savefig('ChemicalReaction.png')
# Close the plot
plt.close()