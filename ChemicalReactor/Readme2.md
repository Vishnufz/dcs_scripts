# Chemical Reaction Simulation

This Python script simulates a batch reactor system undergoing a second-order chemical reaction \(A + B \rightarrow C\).

## Requirements

Numpy
```bash
pip install numpy
```
SciPy
```bash
pip install scipy
```
Matplotlib
```bash
pip install matplotlib
```

## Usage

```python
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
```

### Simulation Parameters

- `t_start`: Start time of the simulation
- `t_end`: End time of the simulation
- `initial_concentration_A`: Initial concentration of reactant A (moles/volume)
- `initial_concentration_B`: Initial concentration of reactant B (moles/volume)
- `initial_concentration_C`: Initial concentration of product C (moles/volume)
- `rate_constant`: Rate constant of the second-order reaction (volume/(moles.time))

```python
t_start = 0                  # Start time
t_end = 100                   # End time
initial_concentration_A = 1.0  # Initial concentration of A (in moles/volume)
initial_concentration_B = 0.5  # Initial concentration of B (in moles/volume)
initial_concentration_C = 0    # Initial concentration of C (in moles/volume)
rate_constant = 0.05          # Rate constant (in volume/(moles.time))
```

### System of Differential Equations

The reaction kinetics are governed by the following system of differential equations:

\[
\begin{align*}
\frac{{d[A]}}{{dt}} &= -k[A][B] \\
\frac{{d[B]}}{{dt}} &= -k[A][B] \\
\frac{{d[C]}}{{dt}} &= k[A][B]
\end{align*}
\]

where \( [A] \), \( [B] \), and \( [C] \) are concentrations of A, B, and C, respectively, and \( k \) is the rate constant.



### Simulation Process

1. Define the system of ODEs representing the reaction kinetics.
2. Set up the initial conditions and time points for simulation.
3. Solve the ODEs numerically using `odeint`.
4. Extract concentrations of A, B, and C from the solution array.
5. Plot the concentrations vs. time using Matplotlib.


