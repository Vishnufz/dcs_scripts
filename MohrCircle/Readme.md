
# Mohr circle & calculation principal stresses

The Mohr Circle,  is a graphical method used in solid mechanics to visualize stress transformation in materials. It provides a geometric representation of how normal and shear stresses change when viewed from different coordinate systems.
By plotting the normal and shear stresses on a graph, the Mohr Circle allows engineers and scientists to determine the principal stresses, maximum shear stress, and the orientation of the principal planes.
## Importing Libraries

The code starts by importing the necessary libraries:

```python
import numpy as np
import matplotlib.pyplot as plt
```

- `numpy` is imported as `np` for numerical computations.
- `matplotlib.pyplot` is imported as `plt` for plotting.

## Defining Functions

### a. `compute_principal_stresses(sigma_x, sigma_y, tau_xy)`

This function computes the principal stresses and the angle of the principal plane.

### b. `plot_mohr_circle(sigma_x, sigma_y, tau_xy)`

This function plots the Mohr circle based on the input stresses.

## Main Function

The code execution begins by calling the `main()` function.

- User inputs the normal stresses `sigma_x` and `sigma_y`, and the shear stress `tau_xy`.
- Principal stresses, the angle of the principal plane, and maximum shear stress are computed by calling the function 
- ```python
    sigma_1, sigma_2, theta_p_deg, tau_max = compute_principal_stresses(sigma_x, sigma_y, tau_xy)

The Mohr circle is plotted based on the input stresses.

    plot_mohr_circle(sigma_x, sigma_y, tau_xy)
