import numpy as np
import matplotlib.pyplot as plt

def compute_principal_stresses(sigma_x, sigma_y, tau_xy):
    # Compute average normal stress
    sigma_avg = (sigma_x + sigma_y) / 2

    # Compute difference in normal stresses
    delta_sigma = (sigma_x - sigma_y) / 2

    # Compute maximum shear stress
    tau_max = np.sqrt(delta_sigma ** 2 + tau_xy ** 2)
    # Compute principal stresses
    sigma_1 = sigma_avg + tau_max
    sigma_2 = sigma_avg - tau_max
    # Compute angle of principal plane
    theta_p = np.arctan2(2 * tau_xy, sigma_x - sigma_y) / 2
    # Convert theta_p to degrees
    theta_p_deg = np.degrees(theta_p)

    return sigma_1, sigma_2, theta_p_deg, tau_max


def plot_mohr_circle(sigma_x, sigma_y, tau_xy):
    # Compute principal stresses
    sigma_1, sigma_2, theta_p_deg, tau_max = compute_principal_stresses(sigma_x, sigma_y, tau_xy)

    # Compute average normal stress
    sigma_avg = (sigma_x + sigma_y) / 2

    # Compute radius of Mohr circle
    delta_sigma = (sigma_x - sigma_y) / 2
    R = tau_max

    # Compute center of Mohr circle
    C = (sigma_avg, 0)

    # Plot Mohr circle
    theta = np.linspace(0, 2 * np.pi, 100)
    circle_x = C[0] + R * np.cos(theta)
    circle_y = R * np.sin(theta)

    plt.figure(figsize=(8, 6))
    plt.plot(circle_x, circle_y, label='Mohr Circle')
    plt.plot(sigma_avg, 0, 'ro', label='Center')
    plt.plot(sigma_x, tau_xy, 'go', label='Point 1')
    plt.plot(sigma_y, -tau_xy, 'bo', label='Point 2')
    plt.plot([sigma_1, sigma_2], [-tau_max, -tau_max], 'kx', label='Principal Stresses')
    plt.plot(sigma_avg, tau_max, 'k+', label='Tau Max')
    plt.text(sigma_1, -tau_max, f'Sigma 1: {sigma_1:.2f} MPa', ha='right', va='bottom')
    plt.text(sigma_2, -tau_max, f'Sigma 2: {sigma_2:.2f} MPa', ha='right', va='bottom')
    plt.text(sigma_avg, tau_max, f'Tau Max: {tau_max:.2f} MPa', ha='right', va='bottom')
    plt.xlabel('Normal Stress (MPa)')
    plt.ylabel('Shear Stress (MPa)')
    plt.title('Mohr Circle')
    plt.axis('equal')
    plt.grid(True)
    plt.legend()
    plt.show()
    # Save the plot as PNG file
    plt.savefig('MohrCircle.png')
    # Close the plot
    plt.close()

def main():
    # Input normal stresses and shear stress
    sigma_x = float(input("Enter normal stress sigma_x (MPa): "))
    sigma_y = float(input("Enter normal stress sigma_y (MPa): "))
    tau_xy = float(input("Enter shear stress tau_xy (MPa): "))

    # Compute and print principal stresses
    sigma_1, sigma_2, theta_p_deg, tau_max = compute_principal_stresses(sigma_x, sigma_y, tau_xy)
    print("Principal Stresses:")
    print("Sigma 1:", sigma_1, "MPa")
    print("Sigma 2:", sigma_2, "MPa")
    print("Principal plane at angle:", theta_p_deg, "degrees")
    print("Tau maximum:", tau_max, "MPa")

    # Plot Mohr circle
    plot_mohr_circle(sigma_x, sigma_y, tau_xy)


if __name__ == "__main__":
    main()