
```python
import pandas as pd
import numpy as np
from collections import Counter
import operator
import math
import matplotlib.pyplot as plt
from statistics import mean
```
 importing necessary libraries. Pandas for data manipulation, NumPy for numerical calculations, Counter and operator for counting occurrences and sorting, math for mathematical functions, and matplotlib.pyplot for plotting.

```python
elements = pd.read_excel("Data/Ex1.xlsx", "Element") # Truss elements
joints = pd.read_excel("Data/Ex1.xlsx", "Joint") # Truss joints
```
loading data from an Excel file into two Pandas DataFrames: `elements` for truss elements and `joints` for truss joints.

```python
X_reactions = joints["RX"].tolist() # Reactions in X direction
Y_reactions = joints["RY"].tolist() # Reactions in y direction
```
converting the "RX" and "RY" columns from the `joints` DataFrame into lists to work with.

```python
x_index = [i for i, x in enumerate(X_reactions) if x == 1]	# Index for reactions in x
y_index = [i for i, x in enumerate(Y_reactions) if x == 1]	# Index for reactions in y
```
finding the indices where the reactions are present in the X and Y directions.

```python
if (len(x_index)) == 1: # 2 Rx and 1 Ry
    x_eq = [1, 0, 0]
    y_eq = [0, 1, 1]
else:                   # 1 Rx and 1 Ry
    x_eq = [1, 1, 0]
    y_eq = [0, 0, 1]
```
defining the equations for equilibrium based on the number of reactions in each direction.

```python
X_values = [-i for i in joints["FX"].tolist()] # Value of the forces in x direction in the other side of =
Y_values = [-i for i in joints["FY"].tolist()] # Value of the forces in y direction in the other side of =
```
Creating lists of forces in the X and Y directions by negating the values from the "FX" and "FY" columns in the `joints` DataFrame.

```python
m_eq = [] # List to save Momentum equation

for i in x_index: # Reaction in X times Y distance to joint A: (0,0)
    m_eq.append(-X_reactions[i]*joints["Y"].tolist()[i])

for j in y_index: # Reaction in Y times X distance to joint A: (0,0)
    m_eq.append(Y_reactions[j]*joints["X"].tolist()[j])
```
Calculating the moments due to reactions about the origin (0,0) for each reaction.

```python
M_F_X = [a*b for a, b in zip(joints["FX"].tolist(), joints["Y"].tolist())] # Momentum of FX forces in the other side of =
M_F_Y = [-a*b for a, b in zip(joints["FY"].tolist(), joints["X"].tolist())] # Momentum of Fxy Forces in the other side of =
```
Calculating the moments due to applied forces about the origin (0,0) for each force.

```python
a = np.array([x_eq, y_eq, m_eq]) # Left side of the equation system
b = np.array([sum(X_values), sum(Y_values), sum(M_F_Y) + sum(M_F_X)]) # Right side of the equation system

R = np.linalg.solve(a, b).tolist() # Reaction solutions R1, R2 and R3
print(a, b, R)
```
Setting up the system of equations in matrix form and solving it to find the reactions. Then, you print the coefficients matrices and the solution.

```python
for i, val_x in enumerate(X_reactions):
    if val_x == 1: 
        X_reactions[i] = round(R[0], 2)
        R.pop(0)

for j, val_y in enumerate(Y_reactions):
    if val_y == 1: 
        Y_reactions[j] = round(R[0], 2)
        R.pop(0)

joints["RX"] = X_reactions
joints["RY"] = Y_reactions
```
Updating the reaction values in the `joints` DataFrame with the calculated values.

```python
joints.set_index("Joint", inplace = True)
```
Setting the "Joint" column as the index of the `joints` DataFrame.

```python
elements["Name"] = [a + b for a, b in zip(elements["Start"], elements["End"])] # Two letters name
elements["Value"] = [None]*len(elements) # Initial values
```
creating a new column "Name" in the `elements` DataFrame which concatenates the "Start" and "End" columns to create a two-letter name for each element. Then, you're initializing a new column "Value" with None values to store the calculated forces later.

```python
letters = Counter(elements["Start"].tolist() + elements["End"].tolist()) # Number of elements by joint
```
using Counter to count the occurrences of joints in both the "Start" and "End" columns of the `elements` DataFrame.

```python
sorted_letters = sorted(letters.items(), key=operator.itemgetter(1)) # Sort the joints by number of elements
sorted_letters = [list(ele) for ele in sorted_letters] # list to tuples
```
sorting the counted joints based on the number of elements connected to them.

```python
while (None in elements["Value"].tolist()):
```
starting a while loop to iterate until all elements have a calculated force value.

```python
    joint = sorted_letters[0][0] # joint with least unknowns
    e_forces = [] # List to save the elements connected to the joint
```
You're getting the joint with the least number of unknowns and initializing an empty list to store the elements connected to that joint.

```python
    for i in range(len(elements)):
        if (joint in elements["Name"][i]) and elements["Value"][i] == None:
            e_forces.append(elements["Name"][i])
```
iterating over elements and adding the names of elements connected to the current joint and with unknown forces to the `e_forces` list.

```python
    if len(e_forces) == 2:
```
checking if the joint has two unknown forces (two elements connected).

```python
        angles = []
        for point in e_forces:
            # Calculation of angles based on joint and connected points
```
alculating the angles of each element connected to the joint.

```python
            # Left side of force equilibrium equations
            left_x = [math.cos(angles[0]), math.cos(angles[1])]
            left_y = [math.sin(angles[0]), math.sin(angles[1])]
            # Right side of force equilibrium equations
            right_x = -(joints.loc[joint]["RX"] + joints.loc[joint]["FX"])
            right_y = -(joints.loc[joint]["RY"] + joints.loc[joint]["FY"])
```
setting up the left and right sides of the force equilibrium equations for the joint with two connected elements.

```python
            a = np.array([left_x, left_y]) # Left side of the equation system
            b = np.array([right_x, right_y]) # Right side of the equation system
```
converting the left and right sides of the equilibrium equations into arrays.

```python
            R = np.linalg.solve(a, b).tolist() # Solution of forces
            result = R.copy() # Store the results
```
solving the equations to find the forces in the elements and storing the results.

```python
            # Update the value of forces in elements
            elements_forces = [tuple(x) for x in list(zip(e_forces, R))]
            for i in e_forces:
                for j, values in enumerate(elements["Name"].tolist()):
                    if i == values:
                        elements["Value"][j] = round(result[0], 2)
                        result.pop(0)
```
 updating the `elements` DataFrame with the calculated forces.

