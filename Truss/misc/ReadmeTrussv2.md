# Truss Structure Analysis and Visualization

This Python script analyzes a truss structure and visualizes the forces acting on each joint and support reactions.

## Function: `solve_truss`

This function solves the truss structure by computing the forces at each joint and support reactions.

### Input Parameters

- `joints`: List of joint coordinates (x, y) in meters.
- `members`: List of truss members, where each member is a tuple `(start_joint, end_joint, length)` with joint indices and length in meters.
- `forces`: List of forces applied at each member in arbitrary units.

### Output

- `joint_forces`: Array of forces at each joint in x and y directions.
- `reactions`: Array of support reactions in x and y directions.

## Function: `visualize_truss`

This function visualizes the truss structure with force components.

### Input Parameters

- `joints`: List of joint coordinates (x, y).
- `members`: List of truss members.
- `forces`: List of forces applied at each member.
- `joint_forces`: Array of forces at each joint.
- `reactions`: Array of support reactions.

### Output

Visualization of the truss structure with force components.