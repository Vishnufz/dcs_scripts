import numpy as np
import matplotlib.pyplot as plt

def solve_truss(joints, members, forces):
    num_joints = len(joints)
    num_members = len(members)
    num_eqs = 2 * num_joints  # Each joint has two equilibrium equations (Fx and Fy)

    # Initialize coefficient matrix and load vector
    A = np.zeros((num_eqs, num_eqs))
    load = np.zeros(num_eqs)

    # Populate coefficient matrix and load vector
    for i, member in enumerate(members):
        start_joint, end_joint, length = member
        start_index = 2 * start_joint
        end_index = 2 * end_joint

        dx = (joints[end_joint][0] - joints[start_joint][0]) / length
        dy = (joints[end_joint][1] - joints[start_joint][1]) / length

        A[start_index, start_index] = dx
        A[start_index, end_index] = -dx
        A[end_index, start_index] = dy
        A[end_index, end_index] = -dy

        load[start_index] -= forces[i] * dx
        load[end_index] -= forces[i] * dy

    # Solve for joint forces using pseudo-inverse to handle singular matrices
    try:
        joint_forces = np.linalg.solve(A, load)
    except np.linalg.LinAlgError:
        joint_forces = np.linalg.pinv(A).dot(load)

    # Calculate support reactions
    reactions = -np.sum(joint_forces.reshape(num_joints, 2), axis=0)

    return joint_forces, reactions

def visualize_truss(joints, members, forces, joint_forces, reactions):
    plt.figure(figsize=(8, 6))

    # Plot joints
    for i, (x, y) in enumerate(joints):
        plt.plot(x, y, 'ko', markersize=8)
        plt.text(x, y, f'Joint {i+1}', fontsize=12, ha='right', va='bottom')

    # Plot members
    for start_joint, end_joint, _ in members:
        start_x, start_y = joints[start_joint]
        end_x, end_y = joints[end_joint]
        plt.plot([start_x, end_x], [start_y, end_y], 'b-', linewidth=2)

    # Plot forces at joints
    for i, (x, y) in enumerate(joints):
        force_x, force_y = joint_forces[2*i], joint_forces[2*i+1]
        plt.arrow(x, y, force_x*0.3, force_y*0.3, head_width=0.1, head_length=0.2, fc='r', ec='r')
        plt.text(x + force_x*0.35, y + force_y*0.35, f'F{i+1} ({force_x:.2f}, {force_y:.2f})', fontsize=10, ha='center', va='center')

    # Plot support reactions
    plt.arrow(0, 0, reactions[0]*0.3, reactions[1]*0.3, head_width=0.1, head_length=0.2, fc='g', ec='g')
    plt.text(reactions[0]*0.35, reactions[1]*0.35, f'Reaction\n({reactions[0]:.2f}, {reactions[1]:.2f})', fontsize=10, ha='center', va='center')

    plt.xlabel('Horizontal Displacement (m)')
    plt.ylabel('Vertical Displacement (m)')
    plt.title('Truss Structure with Force Components')
    plt.grid(True)
    plt.axis('equal')
    plt.show()

# Example usage
joints = [(0, 0), (4, 3), (8, 0)]
members = [(0, 1, 5), (1, 2, 5)]
forces = [10, 15]  # Forces applied at each member (arbitrary units)

joint_forces, reactions = solve_truss(joints, members, forces)
visualize_truss(joints, members, forces, joint_forces, reactions)
print("Joint Forces:")
for i, (force_x, force_y) in enumerate(joint_forces.reshape(-1, 2), start=1):
    print(f"Joint {i}: ({force_x:.2f}, {force_y:.2f})")
print("\nSupport Reactions:")
print(f"Reaction at support 1: ({reactions[0]:.2f}, {reactions[1]:.2f})")
