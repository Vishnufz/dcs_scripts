import numpy as np
import matplotlib.pyplot as plt

# Define parameters
Lx = 1.0  # Length of the domain in the x-direction
Ly = 1.0  # Length of the domain in the y-direction
Nx = 50  # Number of grid points in the x-direction
Ny = 50  # Number of grid points in the y-direction
alpha = 0.01  # Thermal diffusivity

# Discretization
dx = Lx / (Nx - 1)
dy = Ly / (Ny - 1)
dt = 0.0001  # Time step (for steady-state, it's not used)
tolerance = 1e-4  # Convergence tolerance
max_iterations = 5000  # Maximum number of iterations

# Initialize temperature field
T = np.zeros((Nx, Ny))
T_star = np.zeros((Nx, Ny))

# Boundary conditions
T[:, 0] = 0.0  # Left boundary
T[:, -1] = 0.0  # Right boundary
T[0, :] = 0.0  # Bottom boundary
T[-1, :] = 50.0  # Top boundary

# Iterative solution using the finite difference method
for iteration in range(max_iterations):
    T_old = np.copy(T)

    for i in range(1, Nx - 1):
        for j in range(1, Ny - 1):
            T[i, j] = 0.25 * (T[i + 1, j] + T[i - 1, j] + T[i, j + 1] + T[i, j - 1])
            #This was Gaussidel method
            #T[i, j] = 0.25 * (T_old[i + 1, j] + T_old[i - 1, j] + T_old[i, j + 1] + T_old[i, j - 1])
            #Above is Jacobi
            #T_star[i, j] = 0.25 * (T[i + 1, j] + T[i - 1, j] + T[i, j + 1] + T[i, j - 1])
            #T[i, j] = T_old[i, j] + 1.75 * (T_star[i, j]- T_old[i, j])
            # The above method is PSOR

    # Check for convergence
    if np.max(np.abs(T - T_old)) < tolerance:
        print(f"Converged after {iteration} iterations.")
        break

# Plot the results
x = np.linspace(0, Lx, Nx)
y = np.linspace(0, Ly, Ny)
X, Y = np.meshgrid(x, y)

# Save results to a CSV file
np.savetxt("temperature_field.csv", T, delimiter=",")

plt.contourf(X, Y, T, alpha=0.5, cmap='hot')
#plt.contourf(X, Y, T, cmap="hot", levels=50)
plt.colorbar(label="Temperature")
plt.xlabel("X-axis")
plt.ylabel("Y-axis")
plt.title("2D Steady-State Heat Conduction")
plt.show()

