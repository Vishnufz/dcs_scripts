```markdown
# 2D Steady-State Heat Conduction Simulation
simulates 2D steady-state heat conduction using the finite difference method and plots the temperature distribution in the domain.
BY default we use Gauss Seidel method. The user can also use Jacobi method or PSOR
## Parameters

- **Domain Size**:
  - Length of the domain in the x-direction (Lx): 1.0
  - Length of the domain in the y-direction (Ly): 1.0
- **Grid Size**:
  - Number of grid points in the x-direction (Nx): 50
  - Number of grid points in the y-direction (Ny): 50
- **Thermal Diffusivity** (alpha): 0.01
- **Discretization**:
  - Grid spacing in the x-direction (dx): Lx / (Nx - 1)
  - Grid spacing in the y-direction (dy): Ly / (Ny - 1)
  - Time step (dt): 0.0001 (not used for steady-state)
- **Convergence Criteria**:
  - Tolerance for convergence (tolerance): 1e-4
  - Maximum number of iterations (max_iterations): 5000

## Boundary Conditions

- Left boundary: Temperature = 0
- Right boundary: Temperature = 0
- Bottom boundary: Temperature = 0
- Top boundary: Temperature = 50

## Numerical Solution

- Iterative solution using the finite difference method.
- BY default we use Gauss Seidel method. The user can also use Jacobi method or PSOR
- Iterations are performed until the convergence criterion is met or the maximum number of iterations is reached.

## Results

- The temperature field is plotted using contour plot.
- The temperature distribution in the domain is visualized.

## Output

- The temperature field is saved to a CSV file named "temperature_field.csv".

```