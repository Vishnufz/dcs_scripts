import numpy as np
import matplotlib.pyplot as plt

# Define parameters
Lx = 1.0  # Length of the domain in the x-direction
Ly = 1.0  # Length of the domain in the y-direction
Nx = 50   # Number of grid points in the x-direction
Ny = 50   # Number of grid points in the y-direction
alpha = 0.01  # Thermal diffusivity
T_initial = 0.0  # Initial temperature

# Discretization
dx = Lx / (Nx - 1)
dy = Ly / (Ny - 1)
dt = 0.005  # Time step
time_steps = 1000  # Number of time steps

# Initialize temperature field
T = np.ones((Nx, Ny)) * T_initial
laplacian_T =  np.zeros((Nx, Ny))

# Boundary conditions
T[:, 0] = 0.0  # Left boundary
T[:, -1] = 0.0   # Right boundary
T[0, :] = 0.0   # Bottom boundary
T[-1, :] = 100.0  # Top boundary

# Iterative solution using the explicit finite difference scheme
for _ in range(time_steps):
    T_old = np.copy(T)
    #laplacian_T_old = np.copy(laplacian_T)

    for i in range(1, Nx - 1):
        for j in range(1, Ny - 1):
            laplacian_T = (T[i + 1, j] + T[i - 1, j] + T[i, j + 1] + T[i, j - 1] - 4 * T[i, j]) / (dx * dy)
            T[i, j] += alpha * dt * (laplacian_T)


# Plot the results
x = np.linspace(0, Lx, Nx)
y = np.linspace(0, Ly, Ny)
X, Y = np.meshgrid(x, y)

plt.contourf(X, Y, T, cmap="hot",alpha=0.5)
plt.colorbar(label="Temperature")
plt.xlabel("X-axis")
plt.ylabel("Y-axis")
plt.title("Unsteady 2D Heat Conduction")
plt.show()