# Surface Heat Radiation Calculator
calculates the heat radiated from a surface using the Stefan-Boltzmann law.

## Function: `calculate_heat_radiated`

- **Input Parameters**:
  - `surface_temp`: Surface temperature in Kelvin
  - `emissivity`: Emissivity of the surface (between 0 and 1)

- **Output**:
  - `heat_radiated`: Heat radiated from the surface in W/m^2

## Main Function

- The `main()` function prompts the user to input the surface temperature and emissivity.
- It checks if the emissivity is within the valid range (between 0 and 1).
- Calls the `calculate_heat_radiated` function to compute the heat radiated.
- Displays the result.