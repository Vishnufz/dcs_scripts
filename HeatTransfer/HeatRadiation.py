import math


def calculate_heat_radiated(surface_temp, emissivity):
    # Stefan-Boltzmann constant
    sigma = 5.67e-8  # W/m^2 K^4

    # Calculate heat radiated using Stefan-Boltzmann law
    heat_radiated = emissivity * sigma * surface_temp ** 4

    return heat_radiated


def main():
    # Get input from user
    surface_temp = float(input("Enter the surface temperature in Kelvin: "))
    emissivity = float(input("Enter the emissivity of the surface (between 0 and 1): "))

    # Check if emissivity is within valid range
    if emissivity < 0 or emissivity > 1:
        print("Error: Emissivity must be between 0 and 1.")
        return

    # Calculate heat radiated
    heat_radiated = calculate_heat_radiated(surface_temp, emissivity)

    # Display the result
    print("Heat radiated from the surface:", heat_radiated, "W/m^2")


if __name__ == "__main__":
    main()