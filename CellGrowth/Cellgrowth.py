import math
import numpy as np
import matplotlib.pyplot as plt
# Function to calculate final cell count
def calculate_final_cell_count(N0, t, g):
    return N0 * 2**(t / g)

# Function to calculate generation time
def calculate_generation_time(Nt, N0, t):
    return t / (math.log2(Nt) - math.log2(N0))

# Function to calculate growth rate
def calculate_growth_rate(Nt, N0, t):
    return (math.log(Nt) - math.log(N0)) / t

# Function to predict cell growth
def predict_cell_growth():
    # Taking inputs from the user
    N0 = int(input("Enter initial cell count (N0): "))
    t_max = int(input("Enter maximum time interval (hours): "))
    #t = float(input("Enter time interval (hours): "))
    pH = float(input("Enter pH value: "))
    temperature = float(input("Enter temperature (°C): "))
    oxygen_availability = input("Enter oxygen availability (high/low): ").lower()

    # Assuming generation time based on typical conditions
    if temperature >= 37:
        g = 1  # Assuming generation time of 1 hour for higher temperatures
    else:
        g = 2  # Assuming generation time of 2 hours for lower temperatures

    # Adjusting generation time based on pH and oxygen availability
    if pH < 7 or oxygen_availability == "low":
        g *= 1.5  # Increase generation time for suboptimal conditions

    # Calculating final cell count
    Nt = calculate_final_cell_count(N0, t_max, g)

    # Calculating growth rate
    mu = calculate_growth_rate(Nt, N0, t_max)

    # Displaying results
    print("\nResults:")
    print("Final cell count (Nt):", Nt)
    print("Generation time (g):", g)
    print("Growth rate (mu):", mu)

    # Initialize lists to store time and number of cells
    time_points = []
    cell_counts = []

    # Calculate number of cells at different time points
    for t in range(t_max + 1):
        time_points.append(t)
        Nt_t = calculate_final_cell_count(N0, t, g)
        cell_counts.append(Nt_t)

    # Plot number of cells over time
    plt.plot(time_points, cell_counts, marker='o')
    plt.title('Growth of Cells ')
    plt.xlabel('Time (hours)')
    plt.ylabel('Number of Cells')
    # Display exponential growth equation
    #plt.text(sigma_1, -tau_max, f'Sigma 1: {sigma_1:.2f} MPa', ha='right', va='bottom')
    plt.text(0.1, 0.9, f'Generation time: {g}', fontsize=12, transform=plt.gca().transAxes)
    plt.text(0.1, 0.8, f'Growth rate: {round(mu, 2)}', fontsize=12, transform=plt.gca().transAxes)
    plt.grid(True)
    plt.show()
    # Save the plot as PNG file
    plt.savefig('CellGrowthExpLogistic.png')
    # Close the plot
    plt.close()


# Main function
if __name__ == "__main__":
    predict_cell_growth()
