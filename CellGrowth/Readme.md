
# Predict Cell Growth Python Script
This Python script predicts cell growth based on user input parameters such as initial cell count, maximum time interval, pH value, temperature, and oxygen availability. It calculates the final cell count, generation time, and growth rate, and plots the growth curve over time.
Note that in the present code a very crude model is used to compute the growth rate.
To calculate the growth rate detailed input information is required and its case specific, and involves detailed models 
Also exponential growth is considered here. In addition, logistical growth model can be considered bor better analysis

## Step-by-Step Explanation

### Import Required Libraries
The script starts by importing the necessary libraries:
```python
import math
import numpy as np
import matplotlib.pyplot as plt
```

### Define Functions
Several functions are defined to perform calculations and predict cell growth:
- `calculate_final_cell_count`: Calculates the final cell count based on initial cell count, time, and generation time.
- `calculate_generation_time`: Calculates the generation time based on final and initial cell counts, and time.
- `calculate_growth_rate`: Calculates the growth rate based on final and initial cell counts, and time.
- `predict_cell_growth`: Main function to predict cell growth based on user inputs.

### User Inputs
The `predict_cell_growth` function prompts the user to enter the following parameters:
- Initial cell count (`N0`)
- Maximum time interval (`t_max`)
- pH value (`pH`)
- Temperature in Celsius (`temperature`)
- Oxygen availability (`oxygen_availability`)

### Generation Time Calculation
The script calculates the generation time (`g`) based on the provided temperature and adjusts it based on pH and oxygen availability.

### Cell Growth Prediction
Using the calculated generation time (`g`), the script predicts the final cell count (`Nt`) and growth rate (`mu`) using the defined functions.

### Plotting
The script then plots the growth curve over time using Matplotlib. It displays the generation time and growth rate on the plot.

### Save Plot
Finally, the plot is saved as a PNG file.

## Running the Script
To run the script, execute the main function:
```python
if __name__ == "__main__":
    predict_cell_growth()
```

