import math
import numpy as np
import matplotlib.pyplot as plt

# Function to calculate final cell count using exponential growth model
def calculate_final_cell_count_exponential(N0, t, g):
    return N0 * 2**(t / g)

# Function to calculate generation time using exponential growth model
def calculate_generation_time_exponential(Nt, N0, t):
    return t / (math.log2(Nt) - math.log2(N0))

# Function to calculate growth rate using exponential growth model
def calculate_growth_rate_exponential(Nt, N0, t):
    return (math.log(Nt) - math.log(N0)) / t

# Function to calculate final cell count using logistic growth model
def calculate_final_cell_count_logistic(N0, t, K, r):
    return K / (1 + (K / N0 - 1) * math.exp(-r * t))

# Function to predict cell growth using both exponential and logistic growth models
def predict_cell_growth():
    # Taking inputs from the user
    N0 = int(input("Enter initial cell count (N0): "))
    t_max = int(input("Enter maximum time interval (hours): "))
    K = int(input("Enter carrying capacity (K): "))  # Carrying capacity for logistic growth model
    pH = float(input("Enter pH value: "))
    temperature = float(input("Enter temperature (°C): "))
    oxygen_availability = input("Enter oxygen availability (high/low): ").lower()

    # Assuming generation time based on typical conditions
    if temperature >= 37:
        g = 1  # Assuming generation time of 1 hour for higher temperatures
    else:
        g = 2  # Assuming generation time of 2 hours for lower temperatures

    # Adjusting generation time based on pH and oxygen availability for exponential growth model
    if pH < 7 or oxygen_availability == "low":
        g_exp = g * 1.5  # Increase generation time for suboptimal conditions
    else:
        g_exp = g

    # Calculating growth rate for logistic growth model
    Nfinal = calculate_final_cell_count_exponential(N0, t_max, g_exp)
    r = calculate_growth_rate_exponential(Nfinal, N0, t_max) # Growth rate
    r=0.43 # Now this is hard coded here. THis value depends on so many factors and models.

    # Initialize lists to store time and number of cells for both models
    time_points = []
    cell_counts_exp = []
    cell_counts_log = []

    # Calculate number of cells using exponential growth model at different time points
    for t in range(t_max + 1):
        time_points.append(t)
        Nt_t_exp = calculate_final_cell_count_exponential(N0, t, g_exp)
        cell_counts_exp.append(Nt_t_exp)

    # Calculate number of cells using logistic growth model at different time points
    for t in range(t_max + 1):
        Nt_t_log = calculate_final_cell_count_logistic(N0, t, K, r)
        cell_counts_log.append(Nt_t_log)

    # Plot number of cells over time for both models
    plt.plot(time_points, cell_counts_exp, label='Exponential Growth', marker='o')
    plt.plot(time_points, cell_counts_log, label='Logistic Growth', marker='s')
    plt.title('Cell Growth Comparison')
    plt.xlabel('Time (hours)')
    plt.ylabel('Number of Cells')
    plt.legend()
    plt.grid(True)
    plt.show()

# Main function
if __name__ == "__main__":
    predict_cell_growth()