
\section*{Vibration Analysis}

Vibration analysis deals with the study of oscillatory motion of mechanical systems. It involves understanding how and why objects vibrate, what factors influence their vibration, and how to predict and control these vibrations.

\subsection*{One-Dimensional Structures}
In one-dimensional structures, such as bars, beams, and shafts, motion primarily occurs along a single axis. This simplifies the analysis, as we're concerned with displacement, velocity, and acceleration along this axis only.

\subsection*{Types of Vibration}
\begin{itemize}
    \item \textbf{Free Vibration}: Occurs when a system oscillates freely after an initial disturbance, without any external force acting on it.
    \item \textbf{Forced Vibration}: Results from an external force or input applied to the system, causing it to vibrate at a specific frequency.
\end{itemize}


\subsection*{Harmonic Motion}
Many vibrations can be approximated as simple harmonic motion, where the displacement follows a sinusoidal pattern over time.
\begin{itemize}
    \item \textbf{Equation of Motion:} For an object undergoing simple harmonic motion, the displacement \(x\) at time \(t\) can be described by:
    \[ x(t) = A \sin(\omega t + \phi) \]
    where:
    \begin{itemize}
        \item \(A\) is the amplitude of vibration,
        \item \(\omega\) is the angular frequency (\(\omega = 2\pi f\), where \(f\) is the frequency),
        \item \(\phi\) is the phase angle.
    \end{itemize}
    
    \item \textbf{Velocity and Acceleration:}
    \[ v(t) = A\omega \cos(\omega t + \phi) \]
    \[ a(t) = -A\omega^2 \sin(\omega t + \phi) \]
    
    \item \textbf{Time Period (\(T\)):} The time taken for one complete oscillation is the time period, given by:
    \[ T = \frac{2\pi}{\omega} \]
\end{itemize}


\subsection*{ Degrees of Freedom (DOF)}
Degrees of freedom represent the number of independent ways a system can move. In vibration analysis, DOF usually corresponds to the number of independent displacements of the system. For a one-dimensional structure, the degree of freedom (DOF) typically corresponds to the number of independent ways the system can move. In one dimension, this is usually the displacement along that axis.

\subsection*{Natural Frequency}
Natural frequency (\(\omega_n\)) is the frequency at which a system vibrates when subjected to no external forces. It is determined by the system's stiffness (\(k\)) and mass (\(m\)) according to:
\[ \omega_n = \sqrt{\frac{k}{m}} \]

\subsection*{Damping}
Damping refers to the energy dissipation in a vibrating system. It can be modeled using various damping models such as viscous damping, proportional damping, or structural damping.

\subsection*{ Resonance}
Resonance occurs when the frequency of an external force matches the natural frequency of a system, leading to large oscillations and potential structural damage. It is essential to avoid resonance in engineering designs.

\subsection*{Force Balance Equation for Spring-Mass-Damper System}
Consider a spring-mass-damper system subjected to an external force \( F(t) \). The force balance equation for the system can be expressed as:
\[ m \ddot{x}(t) + c \dot{x}(t) + kx(t) = F(t) \]
where:
\begin{itemize}
    \item \( m \) is the mass of the object,
    \item \( c \) is the damping coefficient,
    \item \( k \) is the spring constant, and
    \item \( x(t) \) is the displacement of the mass at time \( t \).
\end{itemize}

\section*{Example: Vibration of a Spring-Mass System}

Consider a simple spring-mass system consisting of a mass $m$ attached to a spring with spring constant $k$. This system is subject to a force $F(t)$, which causes it to vibrate.

\subsection*{Analysis Steps}
\begin{enumerate}
    \item \textbf{Newton's Second Law}: The equation of motion for the system can be derived using Newton's second law:
    \[
    m \cdot \ddot{x}(t) + k \cdot x(t) = F(t)
    \]
    where $x(t)$ is the displacement of the mass at time $t$, and $\ddot{x}(t)$ is its acceleration.
    
    \item \textbf{Free Vibration}: If $F(t) = 0$, the system undergoes free vibration. The solution to the equation of motion yields the natural frequency $\omega_n$ and the mode shapes of vibration.
    
    \item \textbf{Forced Vibration}: When $F(t) \neq 0$, the system experiences forced vibration. The response of the system to the external force depends on its frequency and magnitude.
    
    \item \textbf{Frequency Domain Analysis}: By applying Fourier analysis, we can decompose the forced vibration into its frequency components. This helps in understanding the system's behavior at different frequencies.
    
    \item \textbf{Damping Effects}: In real-world systems, damping (energy dissipation) affects vibration behavior. Damping can be modeled using various damping models, such as viscous damping or structural damping.
    
    \item \textbf{Stability and Resonance}: The system's stability and susceptibility to resonance (excessive vibration amplification) are important considerations in vibration analysis, especially in designing mechanical structures.
\end{enumerate}

\section*{Conclusion}
Vibration analysis provides valuable insights into the dynamic behavior of mechanical systems, helping engineers and researchers optimize designs, mitigate vibration-related issues, and ensure structural integrity and performance. By understanding the fundamental principles and applying appropriate analytical and computational techniques, we can effectively analyze and control vibrations in various engineering applications.



\section*{Vibration analysis of a one-dimensional structure using the finite element method (FEM)}
\subsection*{Mass Matrix $[M]$:}
The mass matrix represents the distribution of mass along the structure and accounts for the inertial effects. For a one-dimensional beam, it can be expressed as:
\[
[M] = \rho A L \begin{bmatrix} 2 & 0 \\ 0 & 1 \end{bmatrix}
\]
Where:
\begin{itemize}
    \item $\rho$ is the material density,
    \item $A$ is the cross-sectional area of the beam,
    \item $L$ is the length of the beam.
\end{itemize}

\subsection*{Stiffness Matrix $[K]$:}
The stiffness matrix describes the resistance of the structure to deformation and is derived from Hooke's Law. For a one-dimensional beam, it can be expressed as:
\[
[K] = \frac{E A}{L} \begin{bmatrix} 1 & -1 \\ -1 & 1 \end{bmatrix}
\]
Where:
\begin{itemize}
    \item $E$ is Young's modulus of the material.
\end{itemize}

\subsection*{Eigenvalue Problem:}
To find the natural frequencies and mode shapes of the structure, we solve the generalized eigenvalue problem:
\[
[K] \mathbf{x} = \omega^2 [M] \mathbf{x}
\]
Where:
\begin{itemize}
    \item $\omega$ is the circular frequency (rad/s),
    \item $\mathbf{x}$ is the eigenvector representing the mode shape.
\end{itemize}

\subsection*{Solution:}
By solving the eigenvalue problem, we obtain the eigenvalues $\omega^2$ and corresponding eigenvectors $\mathbf{x}$. The eigenvalues represent the squared natural frequencies, and the eigenvectors represent the mode shapes.

\section*{Example}
Let's consider a simply supported beam with the following properties:
\begin{itemize}
    \item Length ($L$): 2 meters
    \item Cross-sectional area ($A$): 0.01 $m^2$
    \item Material density ($\rho$): 7850 $kg/m^3$
    \item Young's modulus ($E$): $2 \times 10^{11}$ Pa
\end{itemize}

Using these properties, we can calculate the mass matrix and stiffness matrix. Then, we solve the eigenvalue problem to find the natural frequencies and mode shapes.
