import pandas as pd
import numpy as np
import numpy_financial as npf
import matplotlib.pyplot as plt



def calculate_npv(initial_investment, discount_rate, yearly_net_income, years, yearly_ending_value):
    # Calculate npv
    pv_yearly_net_income = []
    for i in range(0, years):
        pv_yearly_net_income.append( yearly_net_income[i]/(1+discount_rate )**(i+1))

    net_pv_yearly_net_income =sum(pv_yearly_net_income)
    print("Present value net income: ", net_pv_yearly_net_income)
    print("Present value of final ending value: ", yearly_ending_value[years-1]/(1+discount_rate )**(years))
    print("Net Present value : ", (yearly_ending_value[years-1]/(1+discount_rate )**(years) -initial_investment + net_pv_yearly_net_income))




def calculate_roi(initial_investment, yearly_ending_value, yearly_opening_value,years, yearly_net_income):
    yearly_ROI = []
    for i in range(0, years):
        yearly_ROI.append(
            100 * (yearly_ending_value[i] - yearly_opening_value[i] + yearly_net_income[i]) / yearly_opening_value[i])

    print("Yearly ROI:", yearly_ROI)
    print("(For Annual ROI (of each year) it is assumed that Investment is purchased at MV at beginning of that year \n"
          "and sold at MV at end of the same year (after earning the income net of expenses for the year)")
    # print("Yearly cash flow at the end of acj year:", cash_flows)
    # total_cash_flow = sum(cash_flows)
    Final_roi = (yearly_ending_value[-1] - initial_investment + sum(yearly_net_income)) * 100 / initial_investment
    print("Final Return on Investment (ROI): = ",
          (yearly_ending_value[-1] - initial_investment + sum(yearly_net_income)) * 100 / initial_investment, "%")
    return yearly_ROI


def calculate_irr(initial_investment,yearly_return,yearly_ending_value, yearly_expense, years ):
    # Initialize IRR and tolerance
    cash_flowx = [- (initial_investment )]
    for i in range(0, years):
        cash_flowx.append(yearly_return[i] - yearly_expense[i])

    # Adjust cash flows to include the initial investment and ending value
    #cash_flowx[0] -= initial_investment[0]
    cash_flowx[-1] += yearly_ending_value[-1]
    #cash_flowx1 =[-100000,5000,4000,8000,7000,141000] (To test)
    print("Cash flow computed at the beginning of the year:", cash_flowx)
    irr_value = npf.irr(cash_flowx)
    return irr_value



def calculate_cagr(initial_investment, ending_value, years):
    cagr = (ending_value / initial_investment) ** (1 / years) - 1
    return cagr


def main(file_path):
    # Read data from CSV file
    data = pd.read_csv(file_path)

    # Extract data columns
    initial_investment = data['Initial Investment'][0]
    yearly_opening_value = data['Initial Investment'].tolist()
    yearly_ending_value = data['Ending Value'].tolist()
    discount_rate = data['Discount Rate'][0]
    yearly_return = data['Yearly Return'].tolist()
    yearly_expense = data['Yearly Expense'].tolist()
    ending_value = data['Ending Value'][len(data) - 1]

    year = data['Year'].tolist()
    #year = [0] + year
    #print(year)
    years = len(data)
    #print(discount_rate, yearly_expense)

    print("The initial investment is ", initial_investment , "and the number of years is ", years)
    print()
    # Calculate yearly net income
    yearly_net_income = []
    for i in range(0, years):
        yearly_net_income.append( yearly_return[i] - yearly_expense[i])

    average_net_income = sum(yearly_net_income)/years


    # Calculate Net Present Value (NPV)
    calculate_npv(initial_investment, discount_rate, yearly_net_income, years, yearly_ending_value)
    print("Assumed that the investment is purchased at beginning of first year and \n sold at end of final year year at the market value as mentioned in th table")
    print()

    # Calculate cash flows computed at the end of each year (this is not correct approach)
    #cash_flows = []
   # for i in range(0, years):
    #    cash_flows.append(yearly_ending_value[i]-yearly_opening_value[i] + yearly_return[i] - yearly_expense[i])

    # Calculate yearly ROI and ROI at the end of final year
    yearly_ROI = calculate_roi(initial_investment, yearly_ending_value, yearly_opening_value, years, yearly_net_income)
    print( "Actual ROI for the complete period (when investment is purchased at beginning of first year and \n sold at end of final year)")
    print()

    # Calculate the payback period
    print("Payback period: ", initial_investment/average_net_income, "years")
    print("As the annual income from the investment is variable, we are assuming average annual net income for computation of Payback period")
    print()

    # Calculate Internal Rate of return (IRR)
    # Calculate cash flows (cash inflow - cash outflow) at the beginning of year one to the end of last year
    irr_value =calculate_irr(initial_investment,yearly_return,yearly_ending_value, yearly_expense, years )
    print("Internal Rate of return (IRR) ", round(irr_value*100, 2), "%")
    print()

    # Calculate CAGR
    cagr = calculate_cagr(initial_investment, ending_value, years)
    print("Compound Annual Growth Rate (CAGR):", round(cagr*100,2), "%")
    print("(CAGR is computed only on the capital appreciation in the investment i.e, \n on the change in value of invenstement during the complete tenure (Net income ignored))")
    # Calculate actual market value at the end of each year
    actual_market_value_end_of_year = [initial_investment]
    for i in range(1, years+1):
        actual_market_value_end_of_year.append(yearly_ending_value[i-1] )

    #actual_market_value_end_of_year.append(yearly_ending_value[-1])
    #print(actual_market_value_end_of_year)
    # Calculate CAGR based value at the end of each year
    cagr_market_value_end_of_year = [initial_investment]
    for i in range(1, years+1):
        cagr_market_value_end_of_year.append(cagr_market_value_end_of_year[i-1] * (1 + cagr))

    #print(cagr_market_value_end_of_year)
    #print()


    # Plotting
    plt.figure(figsize=(8, 6))
    plt.plot(year, yearly_ROI, marker='*', label='Yearly Return on Investment')
    # plt.plot(year, cagr_market_value_end_of_year, label='CAGR-based Market Value')
    plt.xlabel('Year')
    plt.ylabel('Return on Investment')
    plt.title('Yearly Return on Investment')
    plt.legend()
    plt.grid(True)
    plt.show()
    plt.savefig('ROI.png')
    plt.close()

    year = [0] + year
 # Plotting
    plt.figure(figsize=(8, 6))
    plt.plot(year, actual_market_value_end_of_year,marker='*', label='Actual Market Value')
    plt.plot(year, cagr_market_value_end_of_year, label='CAGR-based Market Value')
    plt.xlabel('Year')
    plt.ylabel('Market Value')
    plt.title('Actual Market Value vs CAGR-based Market Value')
    plt.legend()
    plt.grid(True)
    plt.show()
    plt.savefig('CAGR.png')
    plt.close()

if __name__ == "__main__":
    file_path = "financial_data.csv"  # Path to the CSV file
    main(file_path)
