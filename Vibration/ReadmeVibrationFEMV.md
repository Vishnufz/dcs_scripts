# Vibration Analysis of Beam Structure

This document describes a Python script for performing vibration analysis of a beam structure using finite element methods. The script calculates natural frequencies and mode shapes based on the provided material properties and geometric parameters of the beam.

## Dependencies

The following Python libraries are required to run the script:
- NumPy
- SciPy (specifically `scipy.linalg.eigh`)
- Matplotlib

## Script Overview

The Python script consists of several functions and a main execution block:

1. **assemble_element_matrices**: This function computes the element stiffness and mass matrices based on the given material properties and geometry of the beam.

2. **assemble_global_matrices**: This function assembles the element matrices into global stiffness and mass matrices.

3. **vibration_analysis**: This function solves the eigenvalue problem to obtain the natural frequencies and mode shapes of the beam.

4. **plot_mode_shapes**: This function visualizes the mode shapes of the beam using Matplotlib.

5. **main**: The main function of the script collects input parameters from the user, assembles matrices, performs vibration analysis, and prints the natural frequencies.

## How to Use

To use the script, follow these steps:

1. Ensure that all required dependencies are installed.
2. Run the script `vibration_analysis.py`.
3. Provide input parameters when prompted, including material density, Young's modulus, cross-sectional area, length of the beam, and the number of elements.
4. The script will compute the natural frequencies and display them in Hertz and radians per second.
5. Additionally, it can also plot the mode shapes of the beam.
