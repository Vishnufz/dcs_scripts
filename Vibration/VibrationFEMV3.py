import numpy as np
from scipy.linalg import eigh
import math
from matplotlib import pyplot as plt


def assemble_element_matrices(length, area, density, youngs_modulus,  num_elements ):
    # Element stiffness matrix
    k_element = (num_elements*youngs_modulus * area / length) * np.array([[1, -1], [-1, 1]])
    # note that  we need to take in effect of num of elements here
    # for both the stiffness and mass matrix

    # Element mass matrix
    m_element = density * area * length * np.array([[2, 1], [1, 2]]) / (6.0*num_elements)

    return k_element, m_element

def assemble_global_matrices(num_elements,k_element, m_element):
    # Initialize global matrices
    k_global = np.zeros((num_elements + 1, num_elements + 1))
    m_global = np.zeros((num_elements + 1, num_elements + 1))

    # Assemble global matrices
    for i in range(num_elements):
        k_global[i:i+2, i:i+2] += k_element
        m_global[i:i+2, i:i+2] += m_element

    return k_global, m_global


def vibration_analysis(k_global, m_global):
	restrained_dofs = [0,]

	# remove the fixed degrees of freedom
	for dof in restrained_dofs:
		for i in [0,1]:
			m_global = np.delete(m_global , dof, axis=i)
			k_global = np.delete(k_global, dof, axis=i)

	# eigenvalue problem
	evals, evecs = eigh(k_global,m_global )

	frequencies = np.sqrt(evals)
	return frequencies, evecs


def plot_mode_shapes(evecs):
    num_modes = evecs.shape[1]
    length = len(evecs)
    x = np.linspace(0, length, len(evecs))
    plt.figure(figsize=(10, 6))
    for i in range(num_modes):
        plt.plot(x, evecs[:, i], label=f"Mode {i+1}")

    plt.title("Mode Shapes")
    plt.xlabel("Length of Beam (m)")
    plt.ylabel("Normalized Displacement")
    plt.legend()
    plt.grid(True)
    plt.show()


def main():
    # Input material properties
    density = float(input("Enter material density (kg/m^3): "))
    youngs_modulus = float(input("Enter Young's modulus (N/m^2): "))
    area = float(input("Enter cross-sectional area (m^2): "))
    length = float(input("Enter length (m): "))
    num_elements = int(input("Enter number of elements: "))


    # Assemble element matrices
    k_element, m_element = assemble_element_matrices(length, area, density, youngs_modulus,num_elements)

    # Assemble global matrices
    k_global, m_global = assemble_global_matrices(num_elements, k_element, m_element)

    # Perform vibration analysis
    frequencies, evecs = vibration_analysis(k_global, m_global)


    # Output results
    print("Natural Frequencies (Hz):", frequencies / (2 * np.pi))
    print("Natural Frequencies (rad/s):", frequencies )
   # print("Natural Frequencies (consistent mass) (rad/s):", (5.61/(length))*np.sqrt(youngs_modulus/density), (1.60/(length))*np.sqrt(youngs_modulus/density))
    #print("Natural Frequencies (lumped mass) (rad/s):", (3.69/ (length)) * np.sqrt(youngs_modulus / density), (1.53 / (length)) * np.sqrt(youngs_modulus / density))

if __name__ == "__main__":
    main()