import numpy as np
import matplotlib.pyplot as plt

def eigsort(k, m):
    Omega, vtem = np.linalg.eig(np.linalg.inv(m) @ k)
    wn = np.sort(np.sqrt(Omega))
    isort = np.argsort(Omega)
    v = vtem[:, isort]
    print("The natural frequencies are (rad/sec)")
    print(wn)
    print("\nThe eigenvectors of the system are")
    print(v)
    print("Ratios of eigenvectors are:\n")
    A1_A2_1 = v[0, 0] / v[1, 0]
    A1_A2_2 = v[0, 1] / v[1, 1]
    print("A1/A2 (Mode 1):", A1_A2_1)
    print("A1/A2 (Mode 2):", A1_A2_2)
    plt.figure(1)
    plt.plot([0, 1, 2, 3], [0, A1_A2_1, 1, 0], 'b-s', linewidth=2, markersize=10)
    plt.plot([0, 1, 2, 3], [0, -A1_A2_2, -1, 0], 'r-s', linewidth=2, markersize=10)
    plt.ylabel('Eigen Vector Ratio')
    plt.xlabel('Mass Number')
    plt.xticks([1, 2])
    plt.legend(['Mode 1', 'Mode 2'])
    plt.show()

m1 = 5
m2 = 10
k1 = 1500
k2 = 6500

m = np.array([[m1, 0], [0, m2]])
k = np.array([[k1 + k2, -k2], [-k2, k2]])

eigsort(k, m)
