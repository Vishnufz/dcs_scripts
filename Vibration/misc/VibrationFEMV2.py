import numpy as np
from scipy.linalg import eigh
import math
from matplotlib import pyplot as plt

def bar(num_elems, const_m_matrix, const_k_matrix):
	restrained_dofs = [0,]

	# element mass and stiffness matrices for a bar
	m = const_m_matrix * np.array([[2,1],[1,2]]) / (6. * num_elems)
	k = const_k_matrix * np.array([[1,-1],[-1,1]]) * float(num_elems)

	# construct global mass and stiffness matrices
	M = np.zeros((num_elems+1,num_elems+1))
	K = np.zeros((num_elems+1,num_elems+1))

	# assembly of elements
	for i in range(num_elems):
		M_temp = np.zeros((num_elems+1,num_elems+1))
		K_temp = np.zeros((num_elems+1,num_elems+1))
		M_temp[i:i+2,i:i+2] = m
		K_temp[i:i+2,i:i+2] = k
		M += M_temp
		K += K_temp

	# remove the fixed degrees of freedom
	for dof in restrained_dofs:
		for i in [0,1]:
			M = np.delete(M, dof, axis=i)
			K = np.delete(K, dof, axis=i)

	# eigenvalue problem
	evals, evecs = eigh(K,M)
	frequencies = np.sqrt(evals)
	return M, K, frequencies, evecs


def main():
    # Input material properties
    density = float(input("Enter material density (kg/m^3): "))
    youngs_modulus = float(input("Enter Young's modulus (N/m^2): "))
    area = float(input("Enter cross-sectional area (m^2): "))
    length = float(input("Enter length (m): "))
    num_elements = int(input("Enter number of elements: "))

    const_m_matrix = density * area * length
    const_k_matrix = youngs_modulus * area / length
    M, K, frequencies, evecs = bar(num_elements,const_m_matrix,const_k_matrix)


    # Output results
    print("Natural Frequencies (Hz):", frequencies / (2 * np.pi))
    print("Natural Frequencies (rad/s):", frequencies )
   # print("Natural Frequencies (consistent mass) (rad/s):", (5.61/(length))*np.sqrt(youngs_modulus/density), (1.60/(length))*np.sqrt(youngs_modulus/density))
    #print("Natural Frequencies (lumped mass) (rad/s):", (3.69/ (length)) * np.sqrt(youngs_modulus / density), (1.53 / (length)) * np.sqrt(youngs_modulus / density))

	# Call plot_mode_shapes function
	#plot_mode_shapes(evecs)

    print("Mode Shapes (normalized):")
    for i, mode_shape in enumerate(evecs.T):
        print("Mode", i+1, ":", mode_shape)

if __name__ == "__main__":
    main()