import numpy as np
from scipy.linalg import eig
from scipy.linalg import eigh

def assemble_element_matrices(length, area, density, youngs_modulus):
    # Element stiffness matrix
    k_element = (youngs_modulus * area / length) * np.array([[1, -1], [-1, 1]])
    # note that  we need to take in effect of num of elements here
    # for both the sitffness and mass matrix

    # Element mass matrix
    m_element = density * area * length * np.array([[2, 1], [1, 2]]) / 6.0

    return k_element, m_element

def assemble_global_matrices(num_elements, num_nodes_per_element, k_element, m_element):
    # Initialize global matrices
    k_global = np.zeros((num_elements + 1, num_elements + 1))
    m_global = np.zeros((num_elements + 1, num_elements + 1))

    # Assemble global matrices
    for i in range(num_elements):
        k_global[i:i+2, i:i+2] += k_element
        m_global[i:i+2, i:i+2] += m_element

    return k_global, m_global

def vibration_analysis(k_global, m_global):
    # Solve the eigenvalue problem
    eigvals, eigvecs = eigh(k_global, m_global)

    # Sort eigenvalues and eigenvectors
    idx = eigvals.argsort()
    eigvals = eigvals[idx]
    eigvecs = eigvecs[:, idx]

    # Calculate natural frequencies
    omega = np.sqrt(eigvals)

    return omega, eigvecs

def main():
    # Input material properties
    density = float(input("Enter material density (kg/m^3): "))
    youngs_modulus = float(input("Enter Young's modulus (N/m^2): "))
    area = float(input("Enter cross-sectional area (m^2): "))
    length = float(input("Enter length (m): "))
    num_elements = int(input("Enter number of elements: "))

    # Number of nodes per element
    num_nodes_per_element = 2

    # Assemble element matrices
    k_element, m_element = assemble_element_matrices(length, area, density, youngs_modulus)

    # Assemble global matrices
    k_global, m_global = assemble_global_matrices(num_elements, num_nodes_per_element, k_element, m_element)

    # Perform vibration analysis
    omega, eigvecs = vibration_analysis(k_global, m_global)

    # Output results
    print("Natural Frequencies (Hz):", omega / (2 * np.pi))
    print("Natural Frequencies (rad/s):", omega )
    print("Natural Frequencies (consistent mass) (rad/s):", (5.61/(length))*np.sqrt(youngs_modulus/density), (1.60/(length))*np.sqrt(youngs_modulus/density))
    print("Natural Frequencies (lumped mass) (rad/s):", (3.69/ (length)) * np.sqrt(youngs_modulus / density), (1.53 / (length)) * np.sqrt(youngs_modulus / density))
    print("Mode Shapes (normalized):")
    for i, mode_shape in enumerate(eigvecs.T):
        print("Mode", i+1, ":", mode_shape)

if __name__ == "__main__":
    main()