import numpy as np
from scipy.linalg import eig
import matplotlib.pyplot as plt
def vibration_analysis(mass_matrix, stiffness_matrix):
    # Solve the eigenvalue problem
    eigvals, eigvecs = eig(stiffness_matrix, mass_matrix)

    # Sort eigenvalues and eigenvectors
    idx = eigvals.argsort()
    eigvals = eigvals[idx]
    eigvecs = eigvecs[:, idx]

    # Calculate natural frequencies
    omega = np.sqrt(eigvals)

    # Normalize eigenvectors
    eigvecs /= np.linalg.norm(eigvecs, axis=0)

    # Call plot_mode_shapes function
    plot_mode_shapes(eigvecs)

    return omega, eigvecs

def main():
    # Input material properties
    density = float(input("Enter material density (kg/m^3): "))
    youngs_modulus = float(input("Enter Young's modulus (N/m^2): "))
    area = float(input("Enter cross-sectional area (m^2): "))
    length = float(input("Enter length (m): "))

    # Mass matrix [M]
    mass_matrix = density * area * length * np.array([[2, 0], [0, 1]])

    # Stiffness matrix [K]
    stiffness_matrix = (youngs_modulus * area / length) * np.array([[1, -1], [-1, 1]])

    # Perform vibration analysis
    omega, eigvecs = vibration_analysis(mass_matrix, stiffness_matrix)

    # Output results
    print("Natural Frequencies (Hz):", omega / (2 * np.pi))
    print("Natural Frequencies (Hz):", omega )
    print("Natural Frequencies2 (Hz):", (5.61 / (length)) * np.sqrt(youngs_modulus / density),
          (1.60 / (length)) * np.sqrt(youngs_modulus / density))
    print("Mode Shapes (normalized):")
    for i, mode_shape in enumerate(eigvecs.T):
        print("Mode", i+1, ":", mode_shape)


def plot_mode_shapes(eigvecs):
    num_modes = eigvecs.shape[1]
    length = len(eigvecs)
    x = np.linspace(0, length, len(eigvecs))

    plt.figure(figsize=(10, 6))
    for i in range(num_modes):
        plt.plot(x, eigvecs[:, i], label=f"Mode {i+1}")

    plt.title("Mode Shapes")
    plt.xlabel("Length of Beam (m)")
    plt.ylabel("Normalized Displacement")
    plt.legend()
    plt.grid(True)
    plt.show()





if __name__ == "__main__":
    main()