```markdown
# Vibration Analysis of 1D Bar Element

This Python script performs vibration analysis for a 1D bar element using the Finite Element Method (FEM). It calculates the natural frequencies and mode shapes of the bar element based on user-provided material properties.

## Code Explanation

### Import Libraries

```python
import numpy as np
from scipy.linalg import eig
```

The script imports the necessary libraries, including NumPy for numerical operations and SciPy's `eig` function for solving the eigenvalue problem.

### Assemble Element Matrices

```python
# Function to assemble element matrices
def assemble_element_matrices(length, area, density, youngs_modulus):
    # Element stiffness matrix
    k_element = (youngs_modulus * area / length) * np.array([[1, -1], [-1, 1]])

    # Element mass matrix
    m_element = density * area * length * np.array([[2, 1], [1, 2]]) / 6.0

    return k_element, m_element
```

The `assemble_element_matrices` function calculates the element stiffness matrix (\([K]\)) and element mass matrix (\([M]\)) based on the length, area, density, and Young's modulus of the bar element.

### Assemble Global Matrices

```python
# Function to assemble global matrices
def assemble_global_matrices(num_elements, num_nodes_per_element, k_element, m_element):
    # Initialize global matrices
    k_global = np.zeros((num_elements + 1, num_elements + 1))
    m_global = np.zeros((num_elements + 1, num_elements + 1))

    # Assemble global matrices
    for i in range(num_elements):
        k_global[i:i+2, i:i+2] += k_element
        m_global[i:i+2, i:i+2] += m_element

    return k_global, m_global
```

The `assemble_global_matrices` function assembles the global stiffness matrix (\([K_{global}]\)) and global mass matrix (\([M_{global}]\)) by summing the contributions from each element.

### Vibration Analysis

```python
# Function for vibration analysis
def vibration_analysis(k_global, m_global):
    # Solve the eigenvalue problem
    eigvals, eigvecs = eig(k_global, m_global)

    # Sort eigenvalues and eigenvectors
    idx = eigvals.argsort()
    eigvals = eigvals[idx]
    eigvecs = eigvecs[:, idx]

    # Calculate natural frequencies
    omega = np.sqrt(eigvals)

    return omega, eigvecs
```

The `vibration_analysis` function solves the eigenvalue problem \([K_{global}]\{x\} = \lambda [M_{global}]\{x\}\) to obtain the natural frequencies (\(\omega\)) and mode shapes.

### Main Function

```python
# Main function
def main():
    # Number of nodes per element
    num_nodes_per_element = 2

    # Assemble element matrices
    k_element, m_element = assemble_element_matrices(length, area, density, youngs_modulus)

    # Assemble global matrices
    k_global, m_global = assemble_global_matrices(num_elements, num_nodes_per_element, k_element, m_element)

    # Perform vibration analysis
    omega, eigvecs = vibration_analysis(k_global, m_global)
```
The `main` function prompts the user to input material properties such as density, Young's modulus, area, length, and the number of elements. It then assembles the element matrices, global matrices, and performs vibration analysis using the finite element method. Finally, it outputs the natural frequencies in Hz and rad/s, as well as the mode shapes.
